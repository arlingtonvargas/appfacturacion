﻿<%@ Page Title="Facturas" Language="C#" MasterPageFile="~/master.Master" AutoEventWireup="true" CodeBehind="facturas.aspx.cs" Inherits="appfact.facturas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br>
    <%-- <div class="form-group" style="margin-left: 5%; margin-right: 5%; margin-top: 3%">--%>

    <div class="container">

        <div class="row">
            <div class="col-sm-6">
                <label>Factura #:</label>
                <input type="text" class="form-control" id="txtfactura" readonly />
            </div>
            <div class="col-sm-6">
                <label>Fecha:</label>
                <input type="text" class="form-control" id="txtfecha" readonly />
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <label>Id cliente:</label>
                <input type="text" class="form-control" id="txtidcliente" />
            </div>
            <div class="col-sm-6">
                <label>Nombre :</label>
                <input type="text" class="form-control" id="txtnombrecliente" />
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <label>Dirección :</label>
                <input type="text" class="form-control" id="txtdircliente" />
            </div>
            <div class="col-sm-6">
                <label>Teléfono :</label>
                <input type="number" class="form-control" id="txttelcliente" />
            </div>
        </div>
        <br />

        <div class="row">
            <div class="col-sm-6">
                <label>Id producto :</label>
                <input type="text" class="form-control" id="txtidprod" />
            </div>
            <div class="col-sm-4">
                <label>Cantidad :</label>
                <input type="number" class="form-control" id="txtcantprod" />
            </div>
            <div class="col-sm-2">
                <label>&nbsp;</label>
                <input type="button" id="btnagregar" style="display: grid" class="btn btn-success btn-block" value="Agregar" />
            </div>
        </div>
        <br />


        <table class="table table-hover" id="tablaprod">
            <thead>
                <!-- Cabecera -->
                <tr>
                    <th>Nombre del Producto</th>
                    <!-- Celda de cabecera de la columna 1 -->
                    <th class="auto-style1">Precio</th>
                    <!-- Celda de cabecera de la columna 2 -->
                    <th>Cantidad</th>
                    <!-- Celda de cabecera de la columna 3 -->
                    <th>Total</th>
                    <!-- Celda de cabecera de la columna 4 -->
                </tr>
    
            </thead>
        </table>

        <br />

        <div>
            <label>Subtotal:</label>
            <input type="text" class="form-control" id="textsubtotal" readonly />
        </div>
        <div>
            <label>IVA(19%):</label>
            <input type="text" class="form-control" id="txtiva" readonly />
        </div>
        <div>
            <label>Total a pagar:</label>
            <input type="text" class="form-control" id="txttotal" readonly />
        </div>
        <br />
        <input type="button" id="btnguardar" style="display: grid" class="btn btn-success btn-block" value="Guardar" />
    </div>

    <%-- </div>--%>

    <script>
        $(document).ready(function () {   // Esta parte del código se ejecutará automáticamente cuando la página esté lista.}

            var txtfactura = $('#txtfactura');
            var txtfecha = $('#txtfecha');
            var d = new Date();

            var month = d.getMonth() + 1;
            var day = d.getDate();
            var hora = d.getHours();
            var min = d.getMinutes();
            var sec = d.getSeconds();

            var output = d.getFullYear() + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + day + ' ' +
                (hora < 10 ? '0' : '') + hora + ':' +
                (min < 10 ? '0' : '') + min + ':' +
                (sec < 10 ? '0' : '') + sec;

            txtfecha.val(output);

            $("#btnguardar").click(function () { guardarprod(); });
            $("#btnagregar").click(function () { agregarprod(); });

            function agregarprod() {
                var txtidprod = $('#txtidprod');
                var txtcantprod = $('#txtcantprod');

                if (txtidprod.val() === "") {
                    alert("Debe digitar el id del producto.");
                    txtidprod.focus();
                    return;
                } else if (txtcantprod.val() === "") {
                    alert("Debe ingresar la cantidad.");
                    txtcantprod.focus();
                    return;
                }

                var datosprod =
                {
                    Id : txtidprod.val(),
                    PRNombre: 'kldfsklajsdfklads flkj dafkdas',
                    PRPrecio: 34400
                }

                //YA HIZO LA PETICION AJAX Y DEVOLVIO UN PRODUCTO
                 $("#tablaprod").append('<tr>' +
                        '<td>' + datosprod.PRNombre + '</td>' +
                         '<td>' + datosprod.PRPrecio + '</td>' +
                         '<td>' + txtcantprod.val() + '</td>' +
                         '<td>' + txtcantprod.val() * datosprod.PRPrecio + '</td>' +
                     '</tr>');

            }

            function guardarprod() {

                var txtsubtotal = $('#txtsubtotal');
                var txtiva = $('#txtiva');
                var txttotal = $('#txttotal');
                //if (txtfactura.val() === "") {
                //    alert('El numero de factura no puede estar vacio.');
                //    txtfactura.focus();
                //    return;
                //} else if (txtfactura() === "") {
                //    alert('El numer de factura no puede estar vacio');
                //    return;
                //}
            }
        });






    </script>

</asp:Content>
