﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appfact.Models
{
    public class ProductoET
    {
        public int Id { get; set; }
        public string PRNombre { get; set; }
        public decimal PRPrecio { get; set; }
    }
}