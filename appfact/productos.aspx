﻿<%@ Page Title="Productos" Language="C#" MasterPageFile="~/master.Master" EnableViewState="false" AutoEventWireup="true" CodeBehind="productos.aspx.cs" Inherits="appfact.productos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 200px;
            overflow: auto;
        }

        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

    <div class="form-group" style="margin-left: 5%; margin-right: 5%; margin-top: 3%">
        <div>
            <label>Id:</label>
            <input type="text" class="form-control" id="txtidprod" />
        </div>
        <div>
            <label>Nombre:</label>
            <input type="text" class="form-control" id="txtnomprod" />

        </div>
        <div>
            <label>Precio:</label>
            <input type="number" class="form-control" id="txtprecioprod" />
        </div>
        <br />
        <div>
            <input type="button" id="btnguardar" style="display: grid" class="btn btn-success" value="Guardar" />
        </div>
    </div>
    <div class="container">

        <div class="table-wrapper-scroll-y my-custom-scrollbar">

            <table class="table table-bordered table-striped mb-0" id="tablaprod">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre Producto</th>
                        <th>Precio</th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>
    <script>

        $(document).ready(function () {   // Esta parte del código se ejecutará automáticamente cuando la página esté lista.

            cargarproductos();
            $("#btnguardar").click(function () { guardarprod(); });

            function guardarprod() {

                var txtnombre = $('#txtnomprod');
                var txtprecio = $('#txtprecioprod');
                if (txtnombre.val() === "") {
                    alert('El nombre del producto no puede estar vacío.');
                    txtnombre.focus();
                    return;
                } else if (txtprecio.val() === "") {
                    alert('El precio del producto no puede estar vacío.');
                    txtprecio.focus();
                    return;
                }


                var producto = {};
                producto.Id = 0;
                producto.PRNombre = txtnombre.val();
                producto.PRPrecio = txtprecio.val();

                $.ajax({
                    type: 'POST',
                    url: 'productos.aspx/NuevoProducto',
                    data: "{pProducto:" + JSON.stringify(producto) + "}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (r) {
                        if (r.d === null) {
                            alert("Error al insertar");
                        } else {
                            alert("Se inserto producto");
                            txtnombre.val("");
                            txtprecio.val("");
                            txtnombre.focus();
                            cargarproductos();
                        }
                    }
                });
            }

            function cargarproductos() {
                $.ajax({
                    type: 'POST',
                    url: 'productos.aspx/CargarProductos',
                    data: '',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (r) {
                        if (r.d === null) {
                            console.log("No se encontraron productos");
                        } else {
                            creartabla(r.d);
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            function creartabla(datoslist) {
                $("#tablaprod tbody tr").remove();
                for (i = 0; i < datoslist.length; i++) {

                    $("#tablaprod").append('<tr>' +
                        '<td>' + datoslist[i].Id + '</td>' +
                        '<td>' + datoslist[i].PRNombre + '</td>' +
                        '<td>' + datoslist[i].PRPrecio + '</td>' + '</tr>');
                }
            }

        });




    </script>


    <script type="text/javascript">
        $("#btnCity").live("click", function () {

        });
    </script>



    <div>
        <input type="button" id="btnCity" value="Get City" />
    </div>

</asp:Content>
