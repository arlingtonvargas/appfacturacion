﻿using appfact.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace appfact
{
    public partial class productos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }


        private static ProductoET InsertarProducto(ProductoET pProducto)
        {
            string cad = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection cnn = new SqlConnection(cad))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "Producto_i";

                    cmd.Parameters.AddWithValue("@pPRId", pProducto.Id).Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@pPRNombre", pProducto.PRNombre);
                    cmd.Parameters.AddWithValue("@pPRPrecio", pProducto.PRPrecio);

                    cnn.Open();
                    if (cmd.ExecuteNonQuery() <= 0) return null;

                    pProducto.Id = Convert.ToInt32(cmd.Parameters["@pPRId"].Value);

                    return pProducto;
                }
            }

        }


        private static List<ProductoET> GetProductos()
        {
            string cad = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            List<ProductoET> wProdutos = new List<ProductoET>();
            using (SqlConnection cnn = new SqlConnection(cad))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "Producto_g";
                    cnn.Open();
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        ProductoET producto = new ProductoET();
                        producto.Id = Convert.ToInt32(rd["PRId"]);
                        producto.PRNombre = rd["PRNombre"].ToString();
                        producto.PRPrecio = Convert.ToDecimal(rd["PRPrecio"]);
                        wProdutos.Add(producto);
                    }
                }
            }
            return wProdutos;
        }

        [System.Web.Services.WebMethod]
        public static List<ProductoET> CargarProductos()
        {
            List<ProductoET> res = GetProductos();
            if (res != null && res.Count>0)
                return res;
            else
                return null;
        }


        [System.Web.Services.WebMethod]
        public static ProductoET NuevoProducto(ProductoET pProducto)
        {
            ProductoET res = InsertarProducto(pProducto);
            if (res != null && res.Id > 0)
                return res;
            else
                return null;
        }      
    }
}